<?php
/**
 * @file Drush phpsh commands
 */

/**
 * Implementation of hook_drush_help().
 */
function phpsh_drush_help($section) {
  switch ($section) {
    case 'meta:phpsh:title':
      return dt('phpsh commands');
    case 'meta:phpsh:summary':
      return dt('Commands to help use Drupal from within a phpsh interactive shell.');
  }
}

/**
 * Implementation of hook_drush_command().
 */
function phpsh_drush_command() {
  $items['phpsh-console'] = array(
    'description' => 'Run an interactive shell in the current Drupal environment.',
    'bootstrap' => DRUSH_BOOTSTRAP_DRUPAL_CONFIGURATION,
    'aliases' => array('console', 'c'),
  );
  $items['phpsh-ctags'] = array(
    'description' => 'Generate a CTAGS file for phpsh using exuberant ctags.',
    'bootstrap' => DRUSH_BOOTSTRAP_DRUPAL_CONFIGURATION,
  );
  return $items;
}

/**
 * Start phpsh in the Drupal root with the version specific start script.
 */
function drush_phpsh_console() {
  $commands = drush_get_commands();
  $include = $commands['phpsh-console']['path'] .'/includes/console_'. drush_drupal_major_version() .'.inc';

  // Add some required items to the $_SERVER array, which will be used as the
  // startup environment for phpsh.
  $_SERVER['REMOTE_ADDR'] = '127.0.0.1';
  $_SERVER['SCRIPT_NAME'] = '/index.php';
  $_SERVER['REQUEST_URI'] = '/';
  $_SERVER['HTTP_HOST'] = drush_get_context(DRUSH_URI);

  $root = drush_get_context('DRUSH_DRUPAL_ROOT');
  $command = 'phpsh '. escapeshellarg($include);

  proc_close(proc_open($command, array(0 => STDIN, 1 => STDOUT, 2 => STDERR), $pipes, $root, $_SERVER));
}

/**
 * Generate ctags for Drupal.  Requires exuberant ctags.
 * See http://openflows.com/blog/mvc/2009/01/27/using-exuberant-ctags-drupal
 */
function drush_phpsh_ctags() {
  $root = drush_get_context('DRUSH_DRUPAL_ROOT');
  $command = 'ctags --langmap=php:.engine.inc.module.theme.php.test.install --php-kinds=cdfi --languages=php --recurse';
  proc_close(proc_open($command, array(0 => STDIN, 1 => STDOUT, 2 => STDERR), $pipes, $root));
}
